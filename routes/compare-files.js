const diffService = require('../services/diff-service');
const path = require('path');

module.exports = compareFiles;

function compareFiles (req, res) {
  const path1 = path.resolve(req.files.file1[0].path);
  const path2 = path.resolve(req.files.file2[0].path);

  diffService.compareFiles(path1, path2, result => {
    res.send({ result });
  });
}

compareFiles.multerConfig = [{
  name: 'file1',
  maxCount: 1
}, {
  name: 'file2',
  maxCount: 1
}];