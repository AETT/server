const diffService = require('../services/diff-service');

module.exports = function (req, res) {
  const { source1, source2 } = req.body;
  const result = diffService.compareText(source1, source2);
  res.send({ result });
};