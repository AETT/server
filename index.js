const express = require('express');
const cors = require('cors');
const multer = require('multer');
const bodyParser = require('body-parser');

//routes
const compare = require('./routes/compare');
const compareFiles = require('./routes/compare-files');

const app = express();
const upload = multer({ dest: 'uploads/' });

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Compare files with difflib!')
});
app.post('/compare', compare);
app.post('/compare-files', upload.fields(compareFiles.multerConfig), compareFiles);

app.listen(3001, () => {
  console.log('Server app listening on port 3001');
});