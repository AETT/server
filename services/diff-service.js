var difflib = require('difflib');
const readMultipleFiles = require('read-multiple-files');

const _compare = (source1, source2) => difflib.ndiff(source1.split("\n"), source2.split("\n"));

class DiffService {
  compareText (source1, source2) {
    return _compare(source1, source2);
  }

  compareFiles (file1, file2, callback) {
    readMultipleFiles([file1, file2], 'utf8', (err, result) => {
      // TODO: Handle errors
      callback(_compare(...result));
    });
  }
}

module.exports = new DiffService();
